package ru.tsc.denisturovsky.tm.sevice;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "4321";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "1234567";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";


    @NotNull
    private static final String JMS_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String JMS_HOST_KEY = "jms.host";

    @NotNull
    private static final String JMS_PORT_DEFAULT = "61616";

    @NotNull
    private static final String JMS_PORT_KEY = "jms.port";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "123";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10000";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getDBConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_PASSWORD, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBSecondLvlCache() {
        return getStringValue(DATABASE_SECOND_LVL_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DATABASE_USERNAME, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    @Override
    public String getJMSServerHost() {
        return getStringValue(JMS_HOST_KEY, JMS_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getJMSServerPort() {
        return getStringValue(JMS_PORT_KEY, JMS_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}