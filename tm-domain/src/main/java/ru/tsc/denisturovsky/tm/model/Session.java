package ru.tsc.denisturovsky.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @Column
    @NotNull
    private Date date = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

}