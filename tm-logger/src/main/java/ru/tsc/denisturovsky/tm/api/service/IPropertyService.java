package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getJMSServerHost();

    @NotNull
    String getJMSServerPort();

    @NotNull
    String getMDBServerHost();

    @NotNull
    String getMDBServerPort();

}
